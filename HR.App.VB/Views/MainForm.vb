﻿Imports System.Configuration
Imports SQLite

Public Class MainForm

    Private _connection As SQLiteConnection
    Public ReadOnly Property Database As SQLiteConnection
        Get
            If _connection Is Nothing Then
                _connection = New SQLiteConnection(ConfigurationManager.AppSettings("path-to-database"))
            End If
            Return _connection
        End Get
    End Property

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tbl = Database.Table(Of User)()

        For Each user As User In tbl
            Dim userListitem As New ListViewItem({user.ID.ToString(), user.FirstName, user.LastName})
            UserListView.Items.Add(userListitem)
        Next
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim task2 As New Task2Form
        task2.ShowDialog()
    End Sub

End Class
